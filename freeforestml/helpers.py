import base64
import dill


def python_to_str(obj):
    """
    Convert an arbitrary python object into a string and encode it in base64.
    """
    obj_string = dill.dumps(obj)
    obj_base64 = base64.b64encode(obj_string).decode()
    return obj_base64


def str_to_python(string):
    """
    Reverse of the python_to_str() function.
    """
    obj_string = base64.b64decode(string)
    obj = dill.loads(obj_string)
    return obj
