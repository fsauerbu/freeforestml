
class InvalidProcessSelection(ValueError): pass
class InvalidProcessType(ValueError): pass
class InvalidBlinding(TypeError): pass
class InvalidBins(TypeError): pass
